using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemicSO", menuName = "Scriptables/EnemicSO")]
public class EnemicSO : ScriptableObject
{
    public string m_Nom;
    public float m_Speed;
    public int m_Health;
    public int m_Coin;
}
