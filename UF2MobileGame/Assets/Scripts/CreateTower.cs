using Microsoft.Unity.VisualStudio.Editor;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class CreateTower : MonoBehaviour
{
    [SerializeField]
    private GameObject m_TowerPrefab;
    [SerializeField]
    private GameObject m_TowerPrefab2;
    //Inputs
    [SerializeField]
    private InputActionAsset m_InputSys;
    private InputActionAsset m_Inputs;
    //Posicio del mouse
    private Vector2 m_MousePosition;
    //Tile on es pot posar una torre
    [SerializeField]
    private Tile m_TowerTile;
    //Corrutina MoverTorre
    private Coroutine m_Coroutine;
    //Torre Basica Boton
    [SerializeField]
    private Button m_Button;
    //Torre 2 Boton
    [SerializeField]
    private Button m_Button2;
    //Torre temporal seleccionat
    private GameObject m_GameObjectTorreTemp;
    //TileMap sencer
    [SerializeField]
    private Tilemap m_Tilemap;
    private void Awake()
    {
        m_Inputs = Instantiate(m_InputSys);
        m_Inputs.FindActionMap("Player").Enable();
        m_Inputs.FindActionMap("Player").FindAction("MouseRigthClick").performed += StopPointerTowerDrag;
        m_Inputs.FindActionMap("Player").FindAction("MousePress").performed += StopPointerTowerDrag;
    }
    public void TowerButton() 
    {
        m_GameObjectTorreTemp = Instantiate(m_TowerPrefab);
        m_GameObjectTorreTemp.layer = LayerMask.GetMask("Default");
        m_Coroutine = StartCoroutine(PointerTowerDrag(m_GameObjectTorreTemp));
    }
    public void TowerButton2() 
    {
        m_GameObjectTorreTemp = Instantiate(m_TowerPrefab2);
        m_GameObjectTorreTemp.layer = LayerMask.GetMask("Default");
        m_Coroutine = StartCoroutine(PointerTowerDrag(m_GameObjectTorreTemp));
    }
    IEnumerator PointerTowerDrag(GameObject Tower) 
    {
        while (true)
        {
            yield return new WaitForSeconds(0f);
            Vector2 MousePointer = Camera.main.ScreenToWorldPoint(m_MousePosition);
            Tower.transform.position = MousePointer;
            m_GameObjectTorreTemp.GetComponentInChildren<CircleCollider2D>().enabled = false;
            m_Button.GetComponent<Button>().interactable = false;
            m_Button2.GetComponent<Button>().interactable = false;
        }
    }
    private void StopPointerTowerDrag(InputAction.CallbackContext ctx) 
    {
        if (m_Coroutine !=null) 
        {
            StopCoroutine(m_Coroutine);
            Vector3Int tilemapcenter = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(m_MousePosition));
            if (m_Tilemap.GetTile(tilemapcenter) == m_TowerTile)
            {
                if (!CheckTile())
                {
                    if (GameManager.Instance.Coins >= m_GameObjectTorreTemp.GetComponent<Tower>().TowerSO.m_Price) 
                    {
                        GameManager.Instance.RestarCoins(m_GameObjectTorreTemp.GetComponent<Tower>().TowerSO.m_Price);
                        m_GameObjectTorreTemp.transform.position = m_Tilemap.GetCellCenterWorld(tilemapcenter); 
                    }else
                        Destroy(m_GameObjectTorreTemp);
                }
                else
                {
                    Destroy(m_GameObjectTorreTemp);
                }
            }
            else
            {
                Destroy(m_GameObjectTorreTemp);
            }
            m_Coroutine = null;
            m_GameObjectTorreTemp.GetComponentInChildren<CircleCollider2D>().enabled = true;
            m_GameObjectTorreTemp.layer = 7;
            m_GameObjectTorreTemp = null;
            m_Button.GetComponent<Button>().interactable = true;
            m_Button2.GetComponent<Button>().interactable = true;
        }
    }
    private bool CheckTile() 
    {
        Vector2 MousePointer = Camera.main.ScreenToWorldPoint(m_MousePosition);
        RaycastHit2D hit = Physics2D.Raycast(MousePointer, Vector3.back, Mathf.Infinity, LayerMask.GetMask("Torre"));
        if (hit.collider != null)
        {
            return true;
        }else
            return false;
    }
    private void Update()
    {
        m_MousePosition = m_Inputs.FindActionMap("Player").FindAction("Mouse").ReadValue<Vector2>();
        if (GameManager.Instance.Coins >= m_TowerPrefab.GetComponent<Tower>().TowerSO.m_Price)
        {
            m_Button.interactable =true;
        }
        else
        {
            m_Button.interactable = false;
        }
        if (GameManager.Instance.Coins >= m_TowerPrefab2.GetComponent<Tower>().TowerSO.m_Price)
        {
            m_Button2.interactable = true;
        }
        else
        {
            m_Button2.interactable = false;
        }
    }

}
