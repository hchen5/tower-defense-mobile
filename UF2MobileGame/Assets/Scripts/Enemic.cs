using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;
using static UnityEngine.GraphicsBuffer;

public class Enemic : MonoBehaviour
{
    private Transform[] m_Waypoint;
    private int m_IndexTarget = 0;
    private Rigidbody2D m_Rigidbody;
    [SerializeField]
    private EnemicSO m_EnemicSO;

    private int m_Health = 1;
    private float m_Speed;
    private int m_Coin;
    [SerializeField]
    private SimpleGameEvent m_GameEventEnemyKilled;
    private TextMeshPro m_EnemyVidaText;
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        InitScriptableObject();
        m_EnemyVidaText = transform.GetChild(0).GetComponent<TextMeshPro>();
        UpdateHealthText();
    }
    public void Init(Transform[] waypoint, int healthextra)
    {
        Assert.IsTrue(waypoint != null);
        Assert.IsTrue(waypoint.Length > 0);
        m_Waypoint = waypoint;
        m_IndexTarget = 0;
        transform.position = waypoint[0].position;
        SumarVidaEnemigoPasarRondas(healthextra);
    }
    public void ReturnToPoolEvent() 
    {
        GetComponent<Pooleable>().ReturnToPool();
    }
    private void InitScriptableObject() 
    {
        m_Health = m_EnemicSO.m_Health;
        m_Speed = m_EnemicSO.m_Speed;
        m_Coin = m_EnemicSO.m_Coin;
    }
    private void SumarVidaEnemigoPasarRondas(int healthextra) 
    {
        m_Health = m_EnemicSO.m_Health + healthextra;
        UpdateHealthText();
    }
    public void UpdateHealthText() 
    {
        m_EnemyVidaText.SetText("Vidas: " + m_Health);
    }
    private void FixedUpdate()
    {
        if (m_Waypoint.Length > 0) 
        {
            if (m_IndexTarget !=-1) 
            {
                m_Rigidbody.velocity = (m_Waypoint[m_IndexTarget].position - transform.position).normalized * m_Speed;

                if (Vector3.Distance(transform.position, m_Waypoint[m_IndexTarget].position) < Time.fixedDeltaTime*m_Speed)
                {
                    if (m_Waypoint.Length - 1 == m_IndexTarget)
                    {
                        m_IndexTarget = -1;
                    }
                    else
                    {
                        m_IndexTarget++;
                    }
                }
            }else
                m_Rigidbody.velocity = Vector3.zero;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Bullet")) 
        {
            m_Health--;
            UpdateHealthText();
            if (m_Health == 0) {
                GetComponent<Pooleable>().ReturnToPool();
                m_GameEventEnemyKilled.Raise();
                GameManager.Instance.SumarCoins(m_Coin);
            }
        }
    }
}
