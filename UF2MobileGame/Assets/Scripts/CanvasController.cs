using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_textWave;
    [SerializeField]
    private TextMeshProUGUI m_textCoin;
    public void ChangeToPlayScene()
    {
        SceneManager.LoadScene("Game");
    }
    public void ChangeToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    private void Awake()
    {
        UpdateUI();
    }
    public void UpdateUI()
    {
        if (m_textCoin != null || m_textWave != null) {
            m_textWave.SetText("Total Wave:" + GameManager.Instance.Wave.ToString());
            m_textCoin.SetText("Total Coins:" + GameManager.Instance.Coins.ToString());
        } 
    }
}
