using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEventInt", menuName = "GameEvents/GameEvents/GameEventInt")]
public class GameEventInt : GameEvent<string>{}
