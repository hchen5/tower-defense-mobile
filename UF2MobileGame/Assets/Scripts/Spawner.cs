using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static UnityEditor.Experimental.AssetDatabaseExperimental.AssetDatabaseCounters;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> m_Enemys;
    [SerializeField]
    private float m_SpawnSpeed;
    [SerializeField]
    private Transform[] m_WayPoints;
    [SerializeField]
    private TextMeshProUGUI m_WaveText;
    [SerializeField]
    private TextMeshProUGUI m_TextCountdown;
    //private int m_CurrentWave = 1;
    [SerializeField]
    private int m_InitEnemystoSpawn = 10;
    [SerializeField]
    private float m_MinSpawnSpeed = 0.3f;
    [SerializeField]
    private float m_LessSpawnSpeedEveryWave = 0.1f;
    [SerializeField]
    private float m_MultiplyEnemyToSpawnPerWave = 1.3f;
    private int m_EnemysALive;
    //[SerializeField]
    private List<Pool> m_PoolEnemys;
    [SerializeField]
    private int m_Countdown = 3;
    private void IncrementWave()
    {
        GameManager.Instance.Wave++;
        m_InitEnemystoSpawn = Mathf.RoundToInt((float)(m_InitEnemystoSpawn * m_MultiplyEnemyToSpawnPerWave));
        if (m_SpawnSpeed > m_MinSpawnSpeed) {
            m_SpawnSpeed -= m_LessSpawnSpeedEveryWave;
        }
        m_EnemysALive = m_InitEnemystoSpawn;
        ChangeWaveText();
    }
    private void Awake()
    {
        if(GameManager.Instance != null)
        GameManager.Instance.Wave = 1;
        m_TextCountdown.enabled= false;
        StartCoroutine(Spwaner());
        m_EnemysALive = m_InitEnemystoSpawn;
        ChangeWaveText();
    }
    public void Init(List<Pool> listaenemigos)
    {
        m_PoolEnemys = listaenemigos;
    }
    IEnumerator Spwaner()
    {
        /*
        while (m_Enemys.Count > 0) {

            GameObject enemy = Instantiate(m_Enemys[0], m_WayPoints[0].position, Quaternion.identity);
            enemy.GetComponent<Enemic>().Init(m_WayPoints);
            yield return new WaitForSeconds(m_SpawnSpeed);
        }*/
        if (GameManager.Instance.Wave == 1) {
            int countdw = m_Countdown; //16
            while (countdw > 0)
            {
                countdw--;
                m_TextCountdown.enabled = true;
                m_TextCountdown.SetText("Time to Start: "+countdw.ToString());
                yield return new WaitForSeconds(1);
            }
        }
        StartCoroutine(CloseCountDownText());
        if (m_Enemys.Count > 0)
        {
            int augmentarvidaenemics = (int)GameManager.Instance.Wave / 3;
            for (int i = 0; i < m_InitEnemystoSpawn; i++)
            {
                //GameObject enemy = Instantiate(m_Enemys[Random.Range(0, m_Enemys.Count)], m_WayPoints[0].position, Quaternion.identity);
                GameObject enemys = m_PoolEnemys[Random.Range(0, m_PoolEnemys.Count)].GetElement();
                enemys.GetComponent<Enemic>().Init(m_WayPoints,augmentarvidaenemics);
                yield return new WaitForSeconds(m_SpawnSpeed);
            }
        }
    }
    IEnumerator CloseCountDownText() 
    {
        m_TextCountdown.SetText("0");
        yield return new WaitForSeconds(2);
        m_TextCountdown.enabled = false;
    }
    IEnumerator WaveManager() 
    {
        int countdown = m_Countdown; //16
        while (countdown > 0) 
        {
            m_TextCountdown.enabled = true;
            countdown--;
            m_TextCountdown.SetText("Time to next wave: " + countdown.ToString());
            yield return new WaitForSeconds(1f);
        }
        IncrementWave();
        StartCoroutine(Spwaner());
        yield return new WaitForSeconds(1f);
        m_TextCountdown.enabled = false;
    }
    public void GameEventListenerEnemys() 
    {
        m_EnemysALive--;
        if (m_EnemysALive == 0)
        {
            StartCoroutine(WaveManager());
        }
    }
    private void ChangeWaveText() 
    {
        m_WaveText.SetText("Wave: "+ GameManager.Instance.Wave);
    }
}
