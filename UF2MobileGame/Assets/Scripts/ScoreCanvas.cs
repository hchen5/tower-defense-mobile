using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreCanvas : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_CoinText;

    public void ActualitzarCoinGUI(int coins) 
    {
        m_CoinText.SetText("Coins: "+ coins);
    }
}
