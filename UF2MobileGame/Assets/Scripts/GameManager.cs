using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager m_Instance;
    public static GameManager Instance => m_Instance;
    [SerializeField]
    private int m_CoinsInicials = 20;
    private int m_Coins;
    private ScoreCanvas m_SC;
    public int Coins 
    { 
        get { return m_Coins; }
        set { m_Coins = value;}
    }
    [SerializeField]
    private int m_VidaInicial = 20;
    private int m_Vida;
    public int Vida { get { return m_Vida;} }
    private FinalPoint m_FinalPoint;
    private int m_CurrentWave = 1;
    [SerializeField]
    private List<Pool> m_PoolsEnemy;
    private Spawner m_Spawner;
    [SerializeField]
    private SimpleGameEvent m_ChangedSceneReturnPool;
    [SerializeField]
    private Pool m_PoolBullet;
    private bool m_GameOverScene;
    public bool GameOverScene { get { return m_GameOverScene; } }
    public Pool Bullet { get { return m_PoolBullet; } }
    public int Wave
    {
        get { return m_CurrentWave; }
        set { m_CurrentWave = value; }
    }
    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        m_Vida = m_VidaInicial;
    }
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("OnSceneLoaded: " + scene.name);
        //Debug.Log(mode);
        if (scene.name == "Game")
        {
            m_FinalPoint = FindObjectOfType<FinalPoint>();
            m_FinalPoint.OnRestarVidaEvent += RestarVida;
            m_SC = FindAnyObjectByType<ScoreCanvas>();
            m_Spawner = FindObjectOfType<Spawner>();
            m_Spawner.Init(m_PoolsEnemy);
            m_Vida = m_VidaInicial;
            m_Coins = m_CoinsInicials;
            m_SC.ActualitzarCoinGUI(m_Coins);
            m_CurrentWave = 1;
            m_FinalPoint.UpdateVidaUI();
        }
        else if (m_FinalPoint != null)
        {
            m_FinalPoint.OnRestarVidaEvent -= RestarVida;
            m_FinalPoint = null;
            m_SC = null;
        }
        else
            m_ChangedSceneReturnPool.Raise();

        if (scene.name == "GameOver")
        {
            m_GameOverScene = true;
        }
        else
            m_GameOverScene = false;
    }
    public void RestarVida() 
    {
        if (m_Vida>0) 
        {
            m_Vida--;
            if (m_Vida <=0) 
            {
                SceneManager.LoadScene("GameOver");
            }
        }
    }
    public void SumarCoins(int coins) 
    { 
        m_Coins += coins;
        m_SC.ActualitzarCoinGUI(m_Coins);
    }
    public void RestarCoins(int coins) 
    {
        m_Coins -= coins;
        m_SC.ActualitzarCoinGUI(m_Coins);
    }
}
