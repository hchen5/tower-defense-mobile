using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Rigidbody2D m_Rigidbody2D;
    //private bool m_FollowEne;
    //private GameObject m_GameObjectEnemy;
    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        //m_FollowEne = false;
    }
    private void OnBecameInvisible()
    {
        //Destroy(gameObject);
        gameObject.GetComponent<Pooleable>().ReturnToPool();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Enemic")) 
        {
            //Destroy(gameObject);
            gameObject.GetComponent<Pooleable>().ReturnToPool();
            //m_FollowEne = false;
            //m_GameObjectEnemy = null;
        }
    }
    public void ReturnToPoolGameEvent() 
    {
        GetComponent<Pooleable>().ReturnToPool();
    }
    /*
    private void Update()
    {
        
        if (m_FollowEne && m_GameObjectEnemy != null )
        {
            m_Rigidbody2D.velocity = (m_GameObjectEnemy.transform.position - transform.position).normalized * 30f;
        }
        else
        {
            Destroy(this.gameObject);
            //gameObject.GetComponent<Pooleable>().ReturnToPool();
        }
    }
    public void FollowEnemy(GameObject m_Enemy) 
    {
        m_GameObjectEnemy = m_Enemy;
        m_FollowEne = true;
    }*/
}
