using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalPoint : MonoBehaviour
{
    public delegate void RestarVidaEvent();
    public event RestarVidaEvent OnRestarVidaEvent;
    [SerializeField]
    private SimpleGameEvent m_EliminarEnemicEvent;
    [SerializeField]
    private TextMeshProUGUI m_VidaText;
    [SerializeField]
    private AudioSource m_BonkSound;
    private void Awake()
    {
        m_VidaText.SetText("Vida: " + GameManager.Instance.Vida);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("Enemic")) 
        {
            m_BonkSound.Play();
            OnRestarVidaEvent?.Invoke();
            m_EliminarEnemicEvent.Raise();
            collision.gameObject.GetComponent<Pooleable>().ReturnToPool();
            UpdateVidaUI();
        }
    }
    public void UpdateVidaUI() 
    {
        m_VidaText.SetText("Vida: " + GameManager.Instance.Vida);
    }
}
