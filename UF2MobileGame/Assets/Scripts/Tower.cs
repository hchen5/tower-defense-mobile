using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;

public class Tower : MonoBehaviour
{
    [SerializeField]
    private TowerSO m_TowerSO;
    public TowerSO TowerSO { get { return m_TowerSO; } }
    /*
    [SerializeField]
    private float m_TempsEnTornarAAtacar = 2f;
    [SerializeField]
    private GameObject m_Bullet;
    [SerializeField]
    private float m_TowerRange = 3f;
    */
    [SerializeField]
    private LayerMask m_EnemyLayerMask;
    /*
    [SerializeField]
    private Vector3 m_PosicionTorre;
    */
    private List<GameObject> m_LListaEnemics = new List<GameObject>();
    //private RaycastHit2D[] m_Raycasthit;
    //private Collider2D[] m_Colliders;
    //public bool Enemy = false;
    //public bool Enemy2 = false;
    private void Awake()
    {

        transform.GetChild(0).GetComponent<CircleCollider2D>().radius = m_TowerSO.m_TowerRange;
    }
    private void Start()
    {
        //m_PosicionTorre = transform.position;
        StartCoroutine(EliminarEnemic());
        //StartCoroutine(DetectarEnemigosPrueba());
        //m_Raycasthit = Physics2D.CircleCastAll(m_PosicionTorre, m_TowerRange, transform.position);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemic"))
        {
            m_LListaEnemics.Add(collision.gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemic"))
        {
            m_LListaEnemics.Remove(collision.gameObject);
        }
    }
    IEnumerator EliminarEnemic()
    {
        while (true) 
        {
            if (m_LListaEnemics.Count > 0)
            {
                //GameObject bullet = Instantiate(m_TowerSO.m_Bullet, transform.position, Quaternion.identity);
                GameObject bullet = GameManager.Instance.Bullet.GetElement();
                bullet.transform.position = transform.position;
                bullet.GetComponent<Rigidbody2D>().velocity = (m_LListaEnemics[0].transform.position - transform.position).normalized * 50f;
                //bullet.GetComponent<Bullet>().FollowEnemy(m_LListaEnemics[0]);
            }
            yield return new WaitForSeconds(m_TowerSO.m_TowerSpeed);
        }
    }
    
    private void FixedUpdate()
    {
        if (m_LListaEnemics.Count > 0 )
        {
            Assert.IsTrue(m_LListaEnemics.Count > 0);
            if (m_LListaEnemics[0] != null)
            {
                Assert.IsTrue(m_LListaEnemics[0] != null);
                Vector2 lookat = m_LListaEnemics[0].transform.position - transform.position;
                transform.up = lookat;
            }
            else
            {
                m_LListaEnemics.Clear();
            }

        }
    }
}
