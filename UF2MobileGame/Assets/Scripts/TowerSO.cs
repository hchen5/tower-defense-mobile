using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

[CreateAssetMenu(fileName = "TowerSO", menuName = "Scriptables/TowerSO")]
public class TowerSO : ScriptableObject
{
    public string m_TowerName;
    public int m_Price;
    public Sprite m_Sprite;
    public float m_TowerRange;
    public float m_TowerSpeed;
    public GameObject m_Bullet;
}
